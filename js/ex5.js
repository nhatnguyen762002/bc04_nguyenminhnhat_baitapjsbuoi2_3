function tinhTong() {
  var soValue = document.getElementById("txt-so").value * 1;
  var tongEl = document.getElementById("txt-tong");
  var soChuc, soDonVi, result;

  soChuc = Math.floor(soValue / 10);
  soDonVi = soValue % 10;
  result = soChuc + soDonVi;

  tongEl.innerHTML = `Tổng của 2 ký số là: <span class="text-danger">${result}</span>`;
}
