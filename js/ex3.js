function doiTien() {
  var usdValue = document.getElementById("txt-usd").value * 1;
  var vndEl = document.getElementById("txt-vnd");
  var result;

  result = usdValue * 23500;
  result = new Intl.NumberFormat("vn-VN").format(result);

  vndEl.innerHTML = `Số tiền đổi được là: <span class="text-danger">${result}</span>`;
}
