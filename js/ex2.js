function tinhTrungBinh() {
  var so1 = document.getElementById("txt-so1").value * 1;
  var so2 = document.getElementById("txt-so2").value * 1;
  var so3 = document.getElementById("txt-so3").value * 1;
  var so4 = document.getElementById("txt-so4").value * 1;
  var so5 = document.getElementById("txt-so5").value * 1;
  var trungBinhEl = document.getElementById("txt-trung-binh");
  var result;

  result = (so1 + so2 + so3 + so4 + so5) / 5;

  trungBinhEl.innerHTML = `Trung bình cộng của 5 số là: <span class="text-danger">${result}</span>`;
}
