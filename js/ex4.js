function tinh() {
  var daiValue = document.getElementById("txt-dai").value * 1;
  var rongValue = document.getElementById("txt-rong").value * 1;
  var ketQuaEl = document.getElementById("txt-ket-qua");
  var chuVi, dienTich;

  chuVi = (daiValue + rongValue) * 2;
  dienTich = daiValue * rongValue;

  ketQuaEl.innerHTML = `Kết quả là: <span class="text-danger">Chu vi = ${chuVi}, Diện tích = ${dienTich}</span>`;
}
